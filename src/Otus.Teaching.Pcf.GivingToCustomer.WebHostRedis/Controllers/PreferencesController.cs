﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;

using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data;
using Otus.Teaching.Pcf.GivingToCustomer.WebHostRedis.Models;


namespace Otus.Teaching.Pcf.GivingToCustomer.WebHostRedis.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]/[action]")]
    public class PreferencesController : Controller
    {
        /// <summary>
        /// Предпочтения клиентов
        /// </summary>

        private readonly IDistributedCache _distributedCache;

        public PreferencesController(IDistributedCache distributedCache)
        {
            _distributedCache = distributedCache;
        }

        /// <summary>
        /// Создать список предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> InitiatePreferences()
        {
            var preferences = FakeDataFactory.Preferences;
            await _distributedCache.SetStringAsync(RedisKeys.PreferencesKey, JsonSerializer.Serialize(preferences));

            return Ok();
        }

        /// <summary>
        /// Получить список предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PreferenceResponse>>> GetPreferencesAsync()
        {

            var value = await _distributedCache.GetStringAsync(RedisKeys.PreferencesKey);
            var preferences = JsonSerializer.Deserialize<List<Preference>>(value);

            if (preferences == null)
            {
                return Ok(new List<PreferenceResponse>());
            }

            var response = preferences.Select(x => new PreferenceResponse()
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();

            return Ok(response);
        }

        /// <summary>
        /// Добавить предпочтение
        /// </summary>
        [HttpPost]
        public async Task<ActionResult> CreatePreferenceAsync([FromBody] PreferenceRequest request)
        {
            var value = await _distributedCache.GetStringAsync(RedisKeys.PreferencesKey);
            var preferences = JsonSerializer.Deserialize<List<Preference>>(value);

            if (request == null)
            {
                return BadRequest();
            }

            if (preferences == null)
            {
                return Ok(new List<PreferenceResponse>());
            }

            if (preferences.FirstOrDefault(x => x.Id == request.Id) != null)
            {
                return BadRequest();
            }

            preferences.Add(new Preference()
            {
                Id = request.Id,
                Name = request.Name
            });

            await _distributedCache.SetStringAsync(RedisKeys.PreferencesKey, JsonSerializer.Serialize(preferences));
            return Ok();
        }


    }
}
