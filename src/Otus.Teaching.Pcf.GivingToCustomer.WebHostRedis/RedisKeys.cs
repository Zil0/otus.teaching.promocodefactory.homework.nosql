﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.WebHostRedis.Controllers;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHostRedis
{
    public static class RedisKeys
    {
        public static readonly string PreferencesKey = "Preferences";
    }
}
